// PIN 2 is occupied for a button which will trigger serial sending //

// -------------------------- SETTINGS ---------------------- //
const char    CHANNEL_SEPARATOR      = ';';                   // Char for separating channels in serial data
const byte    OUTPUT_PINS[]          = {3,     4,     5    }; // Output pins that will be used for input serial data
const boolean OUTPUT_PINS_IS_PWM[]   = {false, false, false}; // If use PWM (for each channel as true/false)
const byte    INPUT_PINS[]           = {6,     7,     8    }; // Input pint that will be used for output serial data
const boolean INPUT_PINS_IS_ANALOG[] = {false, false, false}; // If analog input (for each channel as true/false)
// ---------------------------------------------------------- //



// Some preparings:

const byte    OUTPUT_PINS_COUNT = sizeof(OUTPUT_PINS);
const byte    INPUT_PINS_COUNT  = sizeof(INPUT_PINS);

void setup()
{
    Serial.begin(115200);
    for (byte i = 0; i < OUTPUT_PINS_COUNT; i++) {
        pinMode(OUTPUT_PINS[i], OUTPUT);
    }
    for (byte i = 0; i < INPUT_PINS_COUNT; i++) {
        pinMode(INPUT_PINS[i], INPUT);
    }
    attachInterrupt(0, sendSerial, RISING);
}

void loop() {}


// Serial reading:

byte   inputChannel  = 0;
String channelBuffer = "";

void serialEvent()
{
    while (Serial.available()) {
        char inChar = (char) Serial.read();
        if (inChar == CHANNEL_SEPARATOR || inChar == '\n') {
            OUTPUT_PINS_IS_PWM[inputChannel]
                ? analogWrite(OUTPUT_PINS[inputChannel], channelBuffer.toInt())
                : digitalWrite(OUTPUT_PINS[inputChannel], channelBuffer.toInt());
            channelBuffer = "";
            inputChannel = (inChar == '\n') ? 0 : inputChannel + 1;
        } else {
            channelBuffer += inChar;
        }
    }
}

// Serial sending:

unsigned long lastSendTime = 0;

void sendSerial()
{
    if (lastSendTime < millis() - 180) {
        String serialData = "";
        for (byte i = 0; i < INPUT_PINS_COUNT; i++) {
            serialData += String(
                INPUT_PINS_IS_ANALOG[i]
                    ? analogRead(INPUT_PINS[i])
                    : digitalRead(INPUT_PINS[i])
            ) + (i == (INPUT_PINS_COUNT - 1) ? "\n" : String(CHANNEL_SEPARATOR));
        }
        Serial.print(serialData);
        lastSendTime = millis();
    }
}
