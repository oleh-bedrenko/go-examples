/*
 * Speaks with arduino by serial data.
 * Serial data format example: 1;1;0\n
 */

package main

import (
	"fmt"
	"github.com/tarm/serial"
	"time"
	"math/rand"
	"strconv"
)

const (
	ARDUINO_PORT   = "/dev/ttyACM0" // Exapmle: Windows "COM8", Linux "/dev/ttyACM0"
	ARDUINO_BAUD   = 115200
	CHANNELS_COUNT = 3
)

// Processing of incoming message:
func GotSerial (msg []byte) {
	// Write here code to process input seral data. For testing just output:
	fmt.Println(msg)
}

// Monitors for input serial data:
func Monitor(s *serial.Port) {
	var msg []byte
	var value string
	for {
		buf := make([]byte, CHANNELS_COUNT * 4)
		n, err := s.Read(buf)
		if err != nil {
			panic(err)
		}
		for _, symbol := range buf[:n] {
			str := string(symbol)
			if (str != ";" && str != "\n") {
				value += str
				continue
			}
			number, _ := strconv.Atoi(value)
			value = ""
			msg = append(msg, byte(number))
			if str == "\n" {
				GotSerial(msg)
				msg = nil
			}
		}
	}
}

// Send serial data
func SendSerial(s *serial.Port, msg []byte) {
	var strMsg string
	for key, value := range msg {
		strMsg += strconv.Itoa(int(value))
		if (key < CHANNELS_COUNT - 1) {
			strMsg += ";"
		} else {
			strMsg += "\n"
		}
	}
	s.Write([]byte(strMsg))
}

// Random 1/0, Just for testing:
func RandBool() byte {
    rand.Seed(time.Now().UTC().UnixNano())
    return byte(rand.Intn(2))
}

func main() {
    c := &serial.Config{Name: ARDUINO_PORT, Baud: ARDUINO_BAUD}
    s, err := serial.OpenPort(c)
    if err != nil {
        panic(err)
    }
	rand.Seed(time.Now().UTC().UnixNano())
	time.Sleep(time.Second / 2)
	go Monitor(s)
	
	// Send random serial data to arduino:
	var msg []byte
	for {
		msg = append(msg, RandBool())
		if len(msg) == CHANNELS_COUNT {
			SendSerial(s, msg)
			msg = nil
			time.Sleep(time.Millisecond * 100)
		}
	}
}
