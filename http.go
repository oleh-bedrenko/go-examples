/*
 * Listens to some port and gives answers
 */

package main

import (
    "fmt"
    "net/http"
)

const (
	LISTEN_PORT = "8080"
)

func handler(w http.ResponseWriter, r *http.Request) {
	html := "<!DOCTYPE html><html><head></head><body>";
	html += "<h2>Requested path: " + string(r.URL.Path) + "</h2>";
	html += "</body></html>";
    fmt.Fprintf(w, html)
}

func main() {
    http.HandleFunc("/", handler)
    http.ListenAndServe(":" + LISTEN_PORT, nil)
}
