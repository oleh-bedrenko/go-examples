package cellyfish

import (
	"net/http"
	"time"

	"appengine"
	"appengine/datastore"
)

type PageData struct {
	Title string
	User  User
	Data  interface{}
}

type User struct {
	Name       string
	LoginLink  string
	LogoutLink string
}

type Message struct {
	Author  string
	Content string
	Date    time.Time
}

const DATASTORE_IDENT = "cellyfish"

func init() {
	http.HandleFunc("/post", postHandler)
	http.HandleFunc("/",     indexHandler)
}

func getKey(c appengine.Context) *datastore.Key {
	return datastore.NewKey(c, DATASTORE_IDENT, "some_additional_ident", 0, nil)
}
