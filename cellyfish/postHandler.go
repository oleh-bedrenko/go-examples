package cellyfish

import (
	"net/http"
	"time"

	"appengine"
	"appengine/datastore"
	"appengine/user"
)

func postHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	if msg := r.FormValue("content"); len(msg) > 0 {
		g := Message{
			Content: msg,
			Date:    time.Now(),
		}
		if u := user.Current(c); u != nil {
			g.Author = u.String()
		}
		// We set the same parent key on every Greeting entity to ensure each Greeting
		// is in the same entity group. Queries across the single entity group
		// will be consistent. However, the write rate to a single entity group
		// should be limited to ~1/second.
		key := datastore.NewIncompleteKey(c, DATASTORE_IDENT, getKey(c))
		_, err := datastore.Put(c, key, &g)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
	http.Redirect(w, r, "/", http.StatusFound)
}
