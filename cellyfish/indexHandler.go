package cellyfish

import (
	"html/template"
	"net/http"

	"appengine"
	"appengine/datastore"
	"appengine/user"
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	// Ancestor queries, as shown here, are strongly consistent with the High
	// Replication Datastore. Queries that span entity groups are eventually
	// consistent. If we omitted the .Ancestor from this query there would be
	// a slight chance that Greeting that had just been written would not
	// show up in a query.
	q := datastore.NewQuery(DATASTORE_IDENT).Ancestor(getKey(c)).Order("-Date").Limit(20)
	messages := make([]Message, 0, 20)
	if _, err := q.GetAll(c, &messages); err != nil {
		messages[0] = Message{Author: "Google AppSpot", Content: "Data is preparing. Try again later."}
	}
	page := template.Must(template.ParseFiles(
		"static/_base.html",
		"static/index.html",
	))
    data := PageData{Title: "Welcome to CellyFish", Data: messages, User: User{}}
	if u := user.Current(c); u != nil {
		data.User.Name = u.String()
		data.User.LogoutLink, _ = user.LogoutURL(c, r.URL.String())
	} else {
		data.User.LoginLink, _ = user.LoginURL(c, r.URL.String())
	}
	if err := page.Execute(w, data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
