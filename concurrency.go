package main

import ("fmt"; "time")

func main() {

	// Make two channels:
	c1 := make(chan string)
	c2 := make(chan string)

	// Run in new thread:
	go func() {
		for {
			// Put string to channel 1:
			c1 <- "from 1"
			time.Sleep(time.Second * 2)
		}
	}()

	// Run in new thread:
	go func() {
		for {
			// Put string to channel 2:
			c2 <- "from 2"
			time.Sleep(time.Second * 3)
		}
	}()

	// Run in new thread:
	go func() {
		for {
			select {
				case msg1 := <- c1:
					fmt.Println(msg1)
				case msg2 := <- c2:
					fmt.Println(msg2)
			}
		}
	}()

	// Wait keyboard enter to finish:
	var input string
	fmt.Scanln(&input)
}
