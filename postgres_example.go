/*
 * Using Postgres DB
 */

package main;

import (
	"database/sql"
	_ "github.com/lib/pq"
	"fmt"
)

const (
	DB_USER = "postgres"
	DB_NAME = "medpoisk"
	DB_PASS = "111"
)

func main() {
	// Open DB:
	db, err := sql.Open("postgres", "user=" + DB_USER + " dbname=" + DB_NAME + " password=" + DB_PASS)
	if err != nil {
		panic(err)
	}
	defer db.Close() // Close on error/return/end
	
	// Quering DB:	
	med_user_from_id, med_user_to_id := 1, 50	
	rows, err := db.Query("SELECT name FROM public.users WHERE id BETWEEN $1 AND $2", med_user_from_id, med_user_to_id)	
	if err != nil {
		panic(err)
	}
	
	// Parsing results:
	for rows.Next() {
		var name string
		if err := rows.Scan(&name); err != nil {
			panic(err)
		}
		fmt.Printf("%s\n", name)
    }
}
